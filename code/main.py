import numpy as np
from machine import ADC, Pin

class Servomotor:
    """
    Class representing a servomotor.
    """
    def __init__(self, pin):
        """
        Initializes a Servomotor instance with the specified pin and sets the initial angle to 0.
        
        Args:
        - pin: The pin number to which the servomotor is connected.
        """
        self.pin = pin
        self.angle = 0


    def setAngle(self, angle):
        """
        Sets the angle of the servomotor.
        
        Args:
        - angle: The angle to set for the servomotor.
        """
        self.angle = angle


    def getAngle(self):
        """
        Returns the current angle of the servomotor.
        
        Returns:
        - The current angle of the servomotor.
        """
        return self.angle


    def getPin(self):
        """
        Returns the pin number to which the servomotor is connected.
        
        Returns:
        - The pin number of the servomotor.
        """
        return self.pin


    def __str__(self):
        """
        Returns a string representation of the Servomotor instance.
        
        Returns:
        - A string representation of the Servomotor instance.
        """
        return "Servomotor: pin = " + str(self.pin) + ", angle = " + str(self.angle) + "°"


class IRDistanceSensor:
    """
    Class representing an infrared distance sensor.
    """
    def __init__(self, pin):
        """
        Initializes an IRDistanceSensor instance with the specified pin.
        
        Args:
        - pin: The pin number to which the IR distance sensor is connected.
        """
        self.can = ADC(Pin(pin))
        self.can.atten(ADC.ATTN_11DB)
        ADC.width(ADC.WIDTH_10BIT)


    def measure_distance(self):
        """
        Measures the distance using the IR distance sensor and returns the distance in centimeters.
        
        Returns:
        - The measured distance in centimeters.
        """
        ncan = self.can.read()
        dist_cm = 1092.2 * pow(ncan, -0.853)
        return dist_cm


class Kinematics:
    def direct_kinematics(self, angles):
        """
        Calculates the direct kinematics of a robotic arm given the joint angles.
        
        Args:
        - angles: A list of joint angles in radians.
        
        Returns:
        - The position of the end effector in 3D space.
        """
        link_lengths = [45.90, 30.386, 90.226, 90.251, 88.264, 99.687]
        transformations = [np.eye(4)]
        for i in range(len(angles)):
            theta = angles[i]
            if i == 0 or i == 3:
                transformation_i = np.array([[np.cos(theta), -np.sin(theta), 0, link_lengths[i]],
                                            [np.sin(theta), np.cos(theta), 0, 0],
                                            [0, 0, 1, 0],
                                            [0, 0, 0, 1]])
            else:
                transformation_i = np.array([[1, 0, 0, 0],
                                            [0, np.cos(theta), -np.sin(theta), 0],
                                            [0, np.sin(theta), np.cos(theta), 0],
                                            [0, 0, 0, 1]])
            transformation_total = np.dot(transformations[i], transformation_i)
            transformations.append(transformation_total)
        end_effector_position = transformation_total[:3, 3]
        return end_effector_position


    def inverse_kinematics(self, target_position):
        """
        Calculates the inverse kinematics of a robotic arm given the target end effector position.
        
        Args:
        - target_position: The desired position of the end effector in 3D space.
        
        Returns:
        - The joint angles that result in the desired end effector position.
        """
        link_lengths = [45.90, 30.386, 90.226, 90.251, 88.264, 99.687]
        initial_angles = [0, 0, 0, 0, 0, 0]
        initial_position = self.direct_kinematics(initial_angles)
        tolerance = 1e-4
        max_iterations = 1000
        for _ in range(max_iterations):
            current_position = self.direct_kinematics(initial_angles)
            error = np.linalg.norm(target_position - current_position)

            if error < tolerance:
                break
            position_difference = target_position - current_position
            jacobian = np.zeros((3, 6))
            for i in range(len(initial_angles)):
                theta = initial_angles[i]
                d_theta = 1e-6
                d_theta_array = np.zeros(len(initial_angles))
                d_theta_array[i] = d_theta
                position_diff_plus = self.direct_kinematics(initial_angles + d_theta_array)
                position_diff_minus = self.direct_kinematics(initial_angles - d_theta_array)
                jacobian[:, i] = (position_diff_plus - position_diff_minus) / (2 * d_theta)
            pseudo_inverse = np.linalg.pinv(jacobian)
            angle_update = np.dot(pseudo_inverse, position_difference)
            initial_angles += angle_update
        return initial_angles


    def test(self, target_position=np.array([2.0, 1.5, 1.0]), end_effector=[0, 0, 0, 0, 0, 0]):
        """
        Tests the direct_kinematics, inverse_kinematics, and measure_distance functions.
        
        Args:
        - target_position: The desired position of the end effector in 3D space (default: [2.0, 1.5, 1.0]).
        - end_effector: The initial joint angles of the robotic arm (default: [0, 0, 0, 0, 0, 0]).
        """
        end_effector_pos = self.direct_kinematics(end_effector)
        print("End effector position (direct kinematics):", end_effector_pos)
        inverse_angles = self.inverse_kinematics(target_position)
        inverse_angles_deg = np.degrees(inverse_angles)
        inverse_angles_deg = (inverse_angles_deg + 360) % 360
        print("Motor angles (inverse kinematics):", inverse_angles_deg)
    

    def move(self, coordinates = [2.2, 2.2, 2.2]):
        """
        Moves the robotic arm to the specified coordinates.
        
        Args:
        - coordinates: The desired position of the end effector in 3D space (default: [0, 0, 0, 0, 0, 0]).
        """
        ir = IRDistanceSensor(35)
        servo1 = Servomotor(16)
        servo2 = Servomotor(13)
        servo3 = Servomotor(12)
        servo4 = Servomotor(21)
        servo5 = Servomotor(22)
        servo6 = Servomotor(17)
        angles = self.inverse_kinematics(coordinates)
        servo1.setAngle(angles[0])
        servo2.setAngle(angles[1])
        servo3.setAngle(angles[2])
        servo4.setAngle(angles[3])
        servo5.setAngle(angles[4])
        if ir.measure_distance() < 10:
            servo6.setAngle(angles[5] + 90)
        print("Motor angles (inverse kinematics):", angles)
        print("Motor angles (inverse kinematics):", np.degrees(angles))


if __name__ == "__main__":
    try:
        # Test
        c = Kinematics()
        c.test(np.array([2.0, 1.5, 1.0]), [0, 0, 0, 0, 0, 0])
        c.test(np.array([3.0, 1.5, 1.0]), [1.57, 0, 0, 0, 0, 0])
        ir_sensor = IRDistanceSensor(34)  # Instantiates the IR sensor on pin 34
        distance = ir_sensor.measure_distance()  # Measures the distance
        print("Distance measured by the IR sensor:", distance)  # Displays the measured distance
        # test move function
        c.move([2.2, 2.2, 2.2])    
    except KeyboardInterrupt:
        pass
