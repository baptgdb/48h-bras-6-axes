# Robotic Arm 6 Axis

![Vue 1 du bras](images/bras_vue_1.png)

## Table of Contents

- [Introduction](#introduction)
- [Requirements](#requirements)
- [Description](#description)
- [Robotic Arm Composition](#robotic-arm-composition)
- [Requirements](#requirements)
- [Usage](#usage)
- [Code Description](#code-description)
    - [Direct Kinematics](#direct-kinematics)
    - [Inverse Kinematics](#inverse-kinematics)
- [3D Model](#3d-model)
- [Schematic](#schematic)



## Introduction

This project was completed within 48 hours, with a few additional hours during the following two days. The initial development took place from June 5th to June 6th, 2023, and subsequent improvements were made from June 7th to June 8th, 2023. This repository represents the second version of my 6-axis robotic arm.

## Requirements

- This project needs the following components to work properly:
```
    ESP32 WROOM32 microcontroller
    
    6 servomotors (5 x MG996R and 1 x DF9GMS)
    
    1 IR sensor (GP2Y0A21YK0F)
```

## Description

- The code in this repository is written in Python.

- The repository includes the following:
    - **```The code```** for controlling the robotic arm and its components can be found in the code folder.

    - **```The 3D model```** of the articulated robotic arm can be found in the 3D folder.

    - **```The electronic schematic```** of the robotic arm can be found in the schematic folder.

## Robotic Arm Composition

The robotic arm is composed by :
'''
- AC-DC converter (230VAC 50Hz to 5VDC)
- 6 servomotors
- 1 IR sensor
- 1 ESP32 controller
'''

The **ESP32** serves as the controller for the robotic arm. It controls the servomotors and the **IR sensor**. The **ESP32** is powered by the **AC-DC** converter. The servomotors receive **PWM** signals from the ESP32, allowing precise control of their movements. The ESP32 has **16 PWM pins**, enabling control of up to **16 servomotors** (in this project, 6 servomotors are used). The IR sensor is also connected to the ESP32. The IR sensor is used to detect the presence of an object in front of the robotic arm. The ESP32 is programmed to move the robotic arm to a certain position when an object is detected by the IR sensor.


## Requirements

- To run the code in this repository, you need the following:

```
    Python 3.11.3

    NumPy library

    Machine library
```

you can install the NumPy and Machine library using the following command:

```
pip install numpy

pip install machine
```

for this project i use vscode with **pymakr**, but you can use any other IDE like **thonny IDE** and others.

## Usage

- Thee first thing you need to do is to enter the coordinates of the point you want the robotic arm to move to (end effector).
the coordinates are in the format (x, y, z). The code will then move the end effector of the robotic arm to the specified point.

## Code Description

The code in this repository is written in Python.

* There are 8 major functions in the code:
    - direct kinematics :
        * The direct kinematics function takes the angles of the servomotors as input, and returns the coordinates of the end effector of the robotic arm.

    - inverse kinematics :
        * The inverse kinematics function takes the coordinates of the end effector of the robotic arm as input, and returns the angles of all the servomotors.

    - distance measurement :
        * The distance measurement function return the distance between the infrared distance sensor at the end effector and the object in front of it, using the infrared distance sensor.

    - move :
        * The move function takes the coordinates of the point you want the robotic arm to move to as input, and moves the robotic arm to that point.

    - test :
        * The test function is used to test the robotic arm. It moves the robotic arm to a certain point, and then moves it back to its original position.

    - setAngles :
        * The setAngles function is used to set the angles of the servomotors.

    - getAngles :
        * The getAngles function is used to get the angles of the servomotors.

    - setServo :
        * The setServo function is used to set the angle of a single servo.

### Direct Kinematics

This function calculates the direct kinematics of a robotic arm given the joint angles. It uses the Denavit-Hartenberg parameters and homogeneous transformations to determine the position of the end effector in 3D space. Here's a step-by-step explanation:

1. Initialize the lengths of the robot arm links and an empty list for storing the transformations.

2. Iterate over the joint angles:
    a. Retrieve the current joint angle.

    b. If it's the first or fourth joint angle, create a transformation matrix using the cosine and sine of the angle, along with the corresponding link length.

    c. If it's any other joint angle, create a different transformation matrix.

    d. Calculate the total transformation by multiplying the previous transformation with the current transformation.

    e. Store the total transformation in the list of transformations.

3. Extract the position of the end effector from the last transformation matrix.

4. Return the end effector position.
### Inverse Kinematics

This function calculates the inverse kinematics of a robotic arm given the desired target position of the end effector. It uses an iterative approach to find the joint angles that result in the desired end effector position. Here's a step-by-step explanation:

1. Initialize the lengths of the robot arm links, the initial joint angles (usually set to zero), and the initial position of the end effector using the direct_kinematics function.

2. Set a tolerance value to determine convergence and a maximum number of iterations

3. Perform iterations until convergence or reaching the maximum number of iterations:
    a. Calculate the current position of the end effector using the direct_kinematics function.

    b. Calculate the error between the current position and the target position.

    c. If the error is below the tolerance, break out of the loop.

    d. Calculate the difference between the target position and the current position.

    e. Initialize an empty Jacobian matrix.

    f. Iterate over the joint angles:
        - Perturb the angle slightly and calculate the resulting position using the direct_kinematics function.

        - Perturb the angle in the opposite direction and calculate the resulting position.

        - Calculate the column of the Jacobian matrix using the difference between the two positions divided by the perturbation amount.

    - g. Calculate the pseudo-inverse of the Jacobian matrix.

    - h. Calculate the angle update by multiplying the pseudo-inverse with the difference in positions.

    - i. Update the initial joint angles by adding the angle update.

Return the resulting joint angles.


## 3D Model

![Vue 2 du bras](images/bras_vue_2.png)

![Modèle 3D](~/3D/projet_bras_6_axes.f3z)


## Schematic

![Schematic](images/schematic.png)

![Schematic](~/schematic/Robotic_arm.kicad_sch)




